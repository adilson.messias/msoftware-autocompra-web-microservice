package com.msoftware.autocompraweb.vo;

import java.math.BigDecimal;

public class PagSeguroItemVO {

	private String codBarrasItem;
	private String descProd;
	private Integer qtdItens;
	private BigDecimal valorItem;
	

	public PagSeguroItemVO() {

	}


	
	public String getCodBarrasItem() {
		return codBarrasItem;
	}


	public void setCodBarrasItem(String codBarrasItem) {
		this.codBarrasItem = codBarrasItem;
	}


	public String getDescProd() {
		return descProd;
	}


	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}


	public Integer getQtdItens() {
		return qtdItens;
	}


	public void setQtdItens(Integer qtdItens) {
		this.qtdItens = qtdItens;
	}



	public BigDecimal getValorItem() {
		return valorItem;
	}



	public void setValorItem(BigDecimal valorItem) {
		this.valorItem = valorItem;
	}



	
	
}
