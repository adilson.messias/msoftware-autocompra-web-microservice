package com.msoftware.autocompraweb.vo;

public class PagSeguroClienteVO {

	private String nomeCliente;
	private String email;
	private String DDD;
	private String fone;
	

	public PagSeguroClienteVO() {

	}


	public String getNomeCliente() {
		return nomeCliente;
	}


	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getFone() {
		return fone;
	}


	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getDDD() {
		return DDD;
	}


	public void setDDD(String dDD) {
		DDD = dDD;
	}


	
}
