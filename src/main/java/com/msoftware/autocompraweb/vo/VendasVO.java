package com.msoftware.autocompraweb.vo;

import java.util.Date;

public class VendasVO {

	private Integer id;
	private Date data;
	private Long tableName;
	private Integer idCliente;
	private Integer idItens;
	private Double valorTotal;
	
	VendasVO() {
	}

	public VendasVO(Integer id, Date data, Long tableName, Integer idCliente, Integer idItens, Double valorTotal) {
		super();
		this.id = id;
		this.data = data;
		this.tableName = tableName;
		this.idCliente = idCliente;
		this.idItens = idItens;
		this.valorTotal = valorTotal;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getTableName() {
		return tableName;
	}

	public void setTableName(Long tableName) {
		this.tableName = tableName;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdItens() {
		return idItens;
	}

	public void setIdItens(Integer idItens) {
		this.idItens = idItens;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	
	

}
