package com.msoftware.autocompraweb.vo;

public class MonitorVO {

	private Integer id;
	private Integer idCliente;
	private Integer idVenda;
	private Integer codBarrasProd;
	private String descProd;
	private Double precoUnit;
	private Integer qtdItens;
	private Double valorTotal;
	private Integer idItens;
	private Double pesoProd;
	private Double pesoCarrinho;
	
	public MonitorVO() {

	}

	public MonitorVO(Integer id, Integer idCliente, Integer idVenda, Integer codBarrasProd, String descProd,
			Double precoUnit, Integer qtdItens, Double valorTotal, Integer idItens, Double pesoProd,
			Double pesoCarrinho) {
		super();
		this.id = id;
		this.idCliente = idCliente;
		this.idVenda = idVenda;
		this.codBarrasProd = codBarrasProd;
		this.descProd = descProd;
		this.precoUnit = precoUnit;
		this.qtdItens = qtdItens;
		this.valorTotal = valorTotal;
		this.idItens = idItens;
		this.pesoProd = pesoProd;
		this.pesoCarrinho = pesoCarrinho;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdVenda() {
		return idVenda;
	}

	public void setIdVenda(Integer idVenda) {
		this.idVenda = idVenda;
	}

	public Integer getCodBarrasProd() {
		return codBarrasProd;
	}

	public void setCodBarrasProd(Integer codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}

	public String getDescProd() {
		return descProd;
	}

	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Integer getQtdItens() {
		return qtdItens;
	}

	public void setQtdItens(Integer qtdItens) {
		this.qtdItens = qtdItens;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getIdItens() {
		return idItens;
	}

	public void setIdItens(Integer idItens) {
		this.idItens = idItens;
	}

	public Double getPesoProd() {
		return pesoProd;
	}

	public void setPesoProd(Double pesoProd) {
		this.pesoProd = pesoProd;
	}

	public Double getPesoCarrinho() {
		return pesoCarrinho;
	}

	public void setPesoCarrinho(Double pesoCarrinho) {
		this.pesoCarrinho = pesoCarrinho;
	}

	
}
