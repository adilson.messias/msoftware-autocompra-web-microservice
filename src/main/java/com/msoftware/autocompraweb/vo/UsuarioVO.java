package com.msoftware.autocompraweb.vo;

public class UsuarioVO {
	private Integer id;
	private String email;
	private String senha;
	private String matricula;
	private String perfil;
	
	
	public UsuarioVO() {
		
	}
	
	
	public UsuarioVO(String email, String senha, Integer id, String matricula,String perfil) {
		super();
		this.email = email;
		this.senha = senha;
		this.id = id;
		this.matricula= matricula;
		this.perfil=perfil;

	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}


	public String getMatricula() {
		return matricula;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	public String getPerfil() {
		return perfil;
	}


	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	

}
