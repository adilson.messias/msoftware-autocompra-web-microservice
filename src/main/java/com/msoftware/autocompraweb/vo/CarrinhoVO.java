package com.msoftware.autocompraweb.vo;

public class CarrinhoVO {

	private Integer id;
	private Long codBarras;
	private String status;
	

	public CarrinhoVO() {
	}


	public CarrinhoVO(Integer id, Long codBarras, String status) {
		super();
		this.id = id;
		this.codBarras = codBarras;
		this.status = status;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Long getCodBarras() {
		return codBarras;
	}


	public void setCodBarras(Long codBarras) {
		this.codBarras = codBarras;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	

}
