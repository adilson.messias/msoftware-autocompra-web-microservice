package com.msoftware.autocompraweb.vo;

import java.util.Date;

public class CompraVO {

	private Integer id;
	private Long codBarrasProd;
	private String descProd;
	private Double precoUnit;
	private Double pesoProd;
	private Integer qtdProd;
	private Double valorTotal;
	private Long tableName;
	private Integer codCliente;
	private Date data;

	CompraVO() {

	}

	public CompraVO(Integer id, Long codBarrasProd, String descProd, Double precoUnit, Double pesoProd,
			Integer qtdProd, Double valorTotal, Long tableName, Integer codCliente, Date data) {
		super();
		this.id = id;
		this.codBarrasProd = codBarrasProd;
		this.descProd = descProd;
		this.precoUnit = precoUnit;
		this.pesoProd = pesoProd;
		this.qtdProd = qtdProd;
		this.valorTotal = valorTotal;
		this.tableName = tableName;
		this.codCliente = codCliente;
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCodBarrasProd() {
		return codBarrasProd;
	}

	public void setCodBarrasProd(Long codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}

	public String getDescProd() {
		return descProd;
	}

	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Double getPesoProd() {
		return pesoProd;
	}

	public void setPesoProd(Double pesoProd) {
		this.pesoProd = pesoProd;
	}

	public Integer getQtdProd() {
		return qtdProd;
	}

	public void setQtdProd(Integer qtdProd) {
		this.qtdProd = qtdProd;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Long getTableName() {
		return tableName;
	}

	public void setTableName(Long tableName) {
		this.tableName = tableName;
	}

	public Integer getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Integer codCliente) {
		this.codCliente = codCliente;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
