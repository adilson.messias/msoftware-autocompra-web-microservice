package com.msoftware.autocompraweb.vo;

public class ProdutoVO {

	public Integer id;
	public Long codBarrasProd;
	public String descProd;
	public Double precoUnit;
	public Double pesoProd;

	public ProdutoVO() {
	}

	public ProdutoVO(Integer id, Long codBarrasProd, String descProd, Double precoUnit, Double pesoProd) {
		super();
		this.id = id;
		this.codBarrasProd = codBarrasProd;
		this.descProd = descProd;
		this.precoUnit = precoUnit;
		this.pesoProd = pesoProd;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCodBarrasProd() {
		return codBarrasProd;
	}

	public void setCodBarrasProd(Long codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}

	public String getDescProd() {
		return descProd;
	}

	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Double getPesoProd() {
		return pesoProd;
	}

	public void setPesoProd(Double pesoProd) {
		this.pesoProd = pesoProd;
	}

}
