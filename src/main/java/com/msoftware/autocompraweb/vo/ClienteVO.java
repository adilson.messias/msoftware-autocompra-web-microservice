package com.msoftware.autocompraweb.vo;

public class ClienteVO {
	
    private Integer id; 
	private String emailPadrao;
	private String senhaPadrao;
	private String name ;
	private String emailGoogle;
	private String photoUrlGoogle ;
	private String emailFacebook ;
	private String photoUrlFacebook ;
	private String firstName ;
	private String lastName ;
	private String authToken;
	private String provider ;
	
	public ClienteVO() {
		
	}

	public ClienteVO(Integer id, String emailPadrao, String senhaPadrao, String name, String emailGoogle,
			String photoUrlGoogle, String emailFacebook, String photoUrlFacebook, String firstName, String lastName,
			String authToken, String provider) {
		super();
		this.id = id;
		this.emailPadrao = emailPadrao;
		this.senhaPadrao = senhaPadrao;
		this.name = name;
		this.emailGoogle = emailGoogle;
		this.photoUrlGoogle = photoUrlGoogle;
		this.emailFacebook = emailFacebook;
		this.photoUrlFacebook = photoUrlFacebook;
		this.firstName = firstName;
		this.lastName = lastName;
		this.authToken = authToken;
		this.provider = provider;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailPadrao() {
		return emailPadrao;
	}

	public void setEmailPadrao(String emailPadrao) {
		this.emailPadrao = emailPadrao;
	}

	public String getSenhaPadrao() {
		return senhaPadrao;
	}

	public void setSenhaPadrao(String senhaPadrao) {
		this.senhaPadrao = senhaPadrao;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailGoogle() {
		return emailGoogle;
	}

	public void setEmailGoogle(String emailGoogle) {
		this.emailGoogle = emailGoogle;
	}

	public String getPhotoUrlGoogle() {
		return photoUrlGoogle;
	}

	public void setPhotoUrlGoogle(String photoUrlGoogle) {
		this.photoUrlGoogle = photoUrlGoogle;
	}

	public String getEmailFacebook() {
		return emailFacebook;
	}

	public void setEmailFacebook(String emailFacebook) {
		this.emailFacebook = emailFacebook;
	}

	public String getPhotoUrlFacebook() {
		return photoUrlFacebook;
	}

	public void setPhotoUrlFacebook(String photoUrlFacebook) {
		this.photoUrlFacebook = photoUrlFacebook;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	
	
}
