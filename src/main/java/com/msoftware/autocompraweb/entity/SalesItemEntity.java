package com.msoftware.autocompraweb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "tb_sales_item")
public class SalesItemEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "id_cliente")
	private Integer idCliente;

	@Column(name = "id_venda")
	private Integer idVenda;
	
	@Column(name = "cod_barra_prod")
	private Long codBarrasProd;

	@Column(name = "desc_prod")
	private String descProd;

	@Column(name = "preco_unit")
	private Double precoUnit;
	
	@Column(name = "qtd_itens")
	private Integer qtdItens;
	
	@Column(name = "valor_total")
	private Double valorTotal;
	
	@Column(name = "id_itens")
	private Integer idItens;
	
	@Column(name = "peso_prod")
	private Double pesoProd;
	
	@Column(name = "peso_carrinho")
	private Double pesoCarrinho;
	
			
	public SalesItemEntity() {

	}


	public SalesItemEntity(Integer id, Integer idCliente, Integer idVenda, Long codBarrasProd, String descProd,
			Double precoUnit, Integer qtdItens, Double valorTotal, Integer idItens, Double pesoProd,
			Double pesoCarrinho) {
		super();
		this.id = id;
		this.idCliente = idCliente;
		this.idVenda = idVenda;
		this.codBarrasProd = codBarrasProd;
		this.descProd = descProd;
		this.precoUnit = precoUnit;
		this.qtdItens = qtdItens;
		this.valorTotal = valorTotal;
		this.idItens = idItens;
		this.pesoProd = pesoProd;
		this.pesoCarrinho = pesoCarrinho;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public Integer getIdVenda() {
		return idVenda;
	}


	public void setIdVenda(Integer idVenda) {
		this.idVenda = idVenda;
	}


	public Long getCodBarrasProd() {
		return codBarrasProd;
	}


	public void setCodBarrasProd(Long codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}


	public String getDescProd() {
		return descProd;
	}


	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}


	public Double getPrecoUnit() {
		return precoUnit;
	}


	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}


	public Integer getQtdItens() {
		return qtdItens;
	}


	public void setQtdItens(Integer qtdItens) {
		this.qtdItens = qtdItens;
	}


	public Double getValorTotal() {
		return valorTotal;
	}


	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}


	public Integer getIdItens() {
		return idItens;
	}


	public void setIdItens(Integer idItens) {
		this.idItens = idItens;
	}


	public Double getPesoProd() {
		return pesoProd;
	}


	public void setPesoProd(Double pesoProd) {
		this.pesoProd = pesoProd;
	}


	public Double getPesoCarrinho() {
		return pesoCarrinho;
	}


	public void setPesoCarrinho(Double pesoCarrinho) {
		this.pesoCarrinho = pesoCarrinho;
	}

	
}
