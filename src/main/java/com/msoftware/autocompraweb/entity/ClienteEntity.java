package com.msoftware.autocompraweb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "tb_cliente")
public class ClienteEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "id")
	private Integer id;

	@Column(name = "email_padrao")
	private String emailPadrao;

	@Column(name = "senha_padrao")
	private String senhaPadrao;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "email_google")
	private String emailGoogle;
	
	@Column(name = "photo_url_google")
	private String photoUrlGoogle;
	
	@Column(name = "email_facebook")
	private String emailFacebook;
	
	@Column(name = "photo_url_facebook")
	private String photoUrlFacebook;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "auth_token")
	private String authToken;
	
	@Column(name = "provider")
	private String provider;
	
		
	public ClienteEntity() {
		
	}


	public ClienteEntity(Integer id, String emailPadrao, String senhaPadrao, String name, String emailGoogle,
			String photoUrlGoogle, String emailFacebook, String photoUrlFacebook, String firstName, String lastName,
			String authToken,String provider) {
		super();
		this.id = id;
		this.emailPadrao = emailPadrao;
		this.senhaPadrao = senhaPadrao;
		this.name = name;
		this.emailGoogle = emailGoogle;
		this.photoUrlGoogle = photoUrlGoogle;
		this.emailFacebook = emailFacebook;
		this.photoUrlFacebook = photoUrlFacebook;
		this.firstName = firstName;
		this.lastName = lastName;
		this.authToken = authToken;
		this.provider = provider;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getEmailPadrao() {
		return emailPadrao;
	}


	public void setEmailPadrao(String emailPadrao) {
		this.emailPadrao = emailPadrao;
	}


	public String getSenhaPadrao() {
		return senhaPadrao;
	}


	public void setSenhaPadrao(String senhaPadrao) {
		this.senhaPadrao = senhaPadrao;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmailGoogle() {
		return emailGoogle;
	}


	public void setEmailGoogle(String emailGoogle) {
		this.emailGoogle = emailGoogle;
	}


	public String getPhotoUrlGoogle() {
		return photoUrlGoogle;
	}


	public void setPhotoUrlGoogle(String photoUrlGoogle) {
		this.photoUrlGoogle = photoUrlGoogle;
	}


	public String getEmailFacebook() {
		return emailFacebook;
	}


	public void setEmailFacebook(String emailFacebook) {
		this.emailFacebook = emailFacebook;
	}


	public String getPhotoUrlFacebook() {
		return photoUrlFacebook;
	}


	public void setPhotoUrlFacebook(String photoUrlFacebook) {
		this.photoUrlFacebook = photoUrlFacebook;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getAuthToken() {
		return authToken;
	}


	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}


	public String getProvider() {
		return provider;
	}


	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	

}
