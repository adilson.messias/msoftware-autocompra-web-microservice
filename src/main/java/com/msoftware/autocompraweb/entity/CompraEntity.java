package com.msoftware.autocompraweb.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;


public class CompraEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "id")
	private Integer id;

	@Column(name = "cod_barra_prod")
	private Long codBarrasProd;

	@Column(name = "desc_prod")
	private String descProd;

	@Column(name = "preco_unit")
	private Double precoUnit;

	@Column(name = "peso_prod")
	private Double pesoProd;

	@Column(name = "qtd_prod")
	private Integer qtdProd;

	@Column(name = "valor_total")
	private Double valorTotal;

	@Column(name = "table_name")
	private Long tableName;

	@Column(name = "cod_cli")
	private Integer codCliente;

	@Column(name = "data")
	private Date data;

	public CompraEntity() {

	}

	public CompraEntity(Integer id, Long codBarrasProd, String descProd, Double precoUnit, Double pesoProd,
			Integer qtdProd, Double valorTotal, Long tableName, Integer codCliente, Date data) {
		super();
		this.id = id;
		this.codBarrasProd = codBarrasProd;
		this.descProd = descProd;
		this.precoUnit = precoUnit;
		this.pesoProd = pesoProd;
		this.qtdProd = qtdProd;
		this.valorTotal = valorTotal;
		this.tableName = tableName;
		this.codCliente = codCliente;
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCodBarrasProd() {
		return codBarrasProd;
	}

	public void setCodBarrasProd(Long codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}

	public String getDescProd() {
		return descProd;
	}

	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Double getPesoProd() {
		return pesoProd;
	}

	public void setPesoProd(Double pesoProd) {
		this.pesoProd = pesoProd;
	}

	public Integer getQtdProd() {
		return qtdProd;
	}

	public void setQtdProd(Integer qtdProd) {
		this.qtdProd = qtdProd;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Long getTableName() {
		return tableName;
	}

	public void setTableName(Long tableName) {
		this.tableName = tableName;
	}

	public Integer getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Integer codCliente) {
		this.codCliente = codCliente;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
