package com.msoftware.autocompraweb.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "tb_vendas")
public class VendasEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "id")
	private Integer id;

	@Column(name = "data")
	private Date data;
	
	@Column(name = "table_name")
	private Long tableName;

	@Column(name = "id_cliente")
	private Integer idCliente;

	@Column(name = "id_itens")
	private Integer idItens;

	@Column(name = "valor_total")
	private Double valorTotal;

	public VendasEntity() {

	}

	public VendasEntity(Integer id, Date data, Long tableName, Integer idCliente, Integer idItens,
			Double valorTotal) {
		super();
		this.id = id;
		this.data = data;
		this.tableName = tableName;
		this.idCliente = idCliente;
		this.idItens = idItens;
		this.valorTotal = valorTotal;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getTableName() {
		return tableName;
	}

	public void setTableName(Long tableName) {
		this.tableName = tableName;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdItens() {
		return idItens;
	}

	public void setIdItens(Integer idItens) {
		this.idItens = idItens;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	
	
}
