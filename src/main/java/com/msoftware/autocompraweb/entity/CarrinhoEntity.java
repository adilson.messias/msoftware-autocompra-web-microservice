package com.msoftware.autocompraweb.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "tb_carrinho_fisico")
public class CarrinhoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "id")
	private Integer id;

	@Column(name = "cod_barras")
	private Long codBarras;

	@Column(name = "status")
	private String status;

	
	public CarrinhoEntity() {

	}


	public CarrinhoEntity(Integer id, Long codBarras, String status) {
		super();
		this.id = id;
		this.codBarras = codBarras;
		this.status = status;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Long getCodBarras() {
		return codBarras;
	}


	public void setCodBarras(Long codBarras) {
		this.codBarras = codBarras;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	

}
