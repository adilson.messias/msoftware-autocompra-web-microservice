package com.msoftware.autocompraweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoCompraWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoCompraWebApplication.class, args);
	}

}
