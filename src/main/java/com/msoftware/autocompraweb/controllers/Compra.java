package com.msoftware.autocompraweb.controllers;


import java.net.URI;
import java.sql.SQLException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompraweb.service.impl.CompraServiceImpl;
import com.msoftware.autocompraweb.vo.CarrinhoVO;
import com.msoftware.autocompraweb.vo.CompraVO;

@RestController
@RequestMapping(value = "/autocompra/compra")
@CrossOrigin(origins = "*")
public class Compra {

	@Autowired
	private CompraServiceImpl compraService;

	@Transactional
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> addItemCarrinho(@RequestBody CompraVO objVO)
			throws ClassNotFoundException, SQLException {
		this.compraService.addItemCarrinho(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CompraVO>> findAllItems(String table) {
		List<CompraVO> listObjVO = this.compraService.findAllItems(table);
		return ResponseEntity.ok().body(listObjVO);

	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<List<CarrinhoVO>> deleteById(@RequestBody CompraVO objVO) {
		this.compraService.deleteById(objVO);
		return ResponseEntity.noContent().build();

	}
	
	@RequestMapping(method = RequestMethod.OPTIONS)
	public ResponseEntity<Void> updateStatusCarrinho(CarrinhoVO objVO)
			throws ClassNotFoundException, SQLException {
		this.compraService.update(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}
}

