package com.msoftware.autocompraweb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.msoftware.autocompraweb.service.impl.ItemServiceImpl;
import com.msoftware.autocompraweb.vo.ItemVO;



@RestController
@RequestMapping(value = "/autocompra/item")
@CrossOrigin(origins = "*")
public class Item {
	
	@Autowired
	private ItemServiceImpl itemService;
   
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ItemVO>> findItemByCodeBarras(Long code) {
		List<ItemVO> listObjVO = this.itemService.findItemByCodeBarra(code);
		return ResponseEntity.ok().body(listObjVO);

	}
	
		
}
