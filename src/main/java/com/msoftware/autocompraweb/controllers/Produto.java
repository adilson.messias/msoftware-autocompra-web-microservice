package com.msoftware.autocompraweb.controllers;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompraweb.service.impl.ProdutoServiceImpl;
import com.msoftware.autocompraweb.vo.ProdutoVO;

@RestController
@RequestMapping(value = "/autocompra/produto")
@CrossOrigin(origins = "*")
public class Produto {
	
	
	@Autowired
	private ProdutoServiceImpl produtoService;
   
	@Transactional
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert( @RequestBody ProdutoVO objVO) {
		this.produtoService.insert(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoVO>> findAllProdutos() {
		List<ProdutoVO> listObjVO = this.produtoService.findAllProdutos();
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(method = RequestMethod.OPTIONS)
	public ResponseEntity<List<ProdutoVO>> findProdutoById(ProdutoVO objVO) {
		Integer id = objVO.getId();
		List<ProdutoVO> listObjVO = this.produtoService.findProdutoById(id);
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<List<ProdutoVO>> update(@RequestBody ProdutoVO objVO, @PathVariable Integer id) {
		objVO.setId(id);
		this.produtoService.update(objVO);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<ProdutoVO>> deleteById(@PathVariable Integer id) {
		this.produtoService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
