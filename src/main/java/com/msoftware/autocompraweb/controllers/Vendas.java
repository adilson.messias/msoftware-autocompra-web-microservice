package com.msoftware.autocompraweb.controllers;

import java.net.URI;
import java.sql.SQLException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompraweb.service.impl.VendasServiceImpl;
import com.msoftware.autocompraweb.vo.VendasVO;

@RestController
@RequestMapping(value = "/autocompra/vendas")
@CrossOrigin(origins = "*")
public class Vendas {
	
	@Autowired
	private VendasServiceImpl vendasService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<VendasVO>> findAllVendas() {
		List<VendasVO> listObjVO = this.vendasService.findAllVendas();
		return ResponseEntity.ok().body(listObjVO);

	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> addItemCarrinho(@RequestBody VendasVO objVO)
			throws ClassNotFoundException, SQLException {
		this.vendasService.insert(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}

	

}
