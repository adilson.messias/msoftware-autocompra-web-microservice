package com.msoftware.autocompraweb.controllers;

import java.net.URI;
import java.sql.SQLException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompraweb.service.impl.CarrinhoServiceImpl;
import com.msoftware.autocompraweb.vo.CarrinhoVO;

@RestController
@RequestMapping(value = "/autocompra/carrinho")
@CrossOrigin(origins = "*")
public class Carrinho {
	
	@Autowired
	private CarrinhoServiceImpl carrinhoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CarrinhoVO>> findAllCarrinho(Long table) {
		List<CarrinhoVO> listObjVO = this.carrinhoService.findAllCarrinho(table);
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Void> insert(@RequestBody Long table) throws ClassNotFoundException, SQLException {
		this.carrinhoService.insert(table);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}
	
	@RequestMapping(method = RequestMethod.PUT)
	@Transactional
	public ResponseEntity<Void> save(@RequestBody CarrinhoVO objVO)  {
		this.carrinhoService.update(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}
	
	@RequestMapping(method = RequestMethod.OPTIONS)
	public ResponseEntity<List<CarrinhoVO>> findAll() {
		List<CarrinhoVO> listObjVO = this.carrinhoService.findAll();
		return ResponseEntity.ok().body(listObjVO);

	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<CarrinhoVO>> deleteById(@PathVariable Integer id) {
		this.carrinhoService.deleteById(id);
		return ResponseEntity.noContent().build();

	}
	
}
