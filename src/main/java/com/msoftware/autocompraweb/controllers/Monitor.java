package com.msoftware.autocompraweb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.msoftware.autocompraweb.service.impl.MonitorServiceImpl;
import com.msoftware.autocompraweb.vo.CarrinhoVO;
import com.msoftware.autocompraweb.vo.CompraVO;

@RestController
@RequestMapping(value = "/autocompra/monitor")
@CrossOrigin(origins = "*")
public class Monitor {

	@Autowired
	private MonitorServiceImpl monitorService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CarrinhoVO>> findAllOperante() {
		List<CarrinhoVO> listObjVO = this.monitorService.findAllOperante();
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(method = RequestMethod.OPTIONS)
	public ResponseEntity<List<CompraVO>> findByItem(String table) {
		List<CompraVO> listObjVO = this.monitorService.findAllItems(table);
		return ResponseEntity.ok().body(listObjVO);

	}

}
