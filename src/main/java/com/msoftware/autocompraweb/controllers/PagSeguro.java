package com.msoftware.autocompraweb.controllers;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.msoftware.autocompraweb.vo.PagSeguroItemVO;

import br.com.uol.pagseguro.domain.AccountCredentials;
import br.com.uol.pagseguro.domain.Credentials;
import br.com.uol.pagseguro.domain.Item;
import br.com.uol.pagseguro.domain.PaymentRequest;
import br.com.uol.pagseguro.domain.Phone;
import br.com.uol.pagseguro.domain.Sender;
import br.com.uol.pagseguro.enums.Currency;
import br.com.uol.pagseguro.exception.PagSeguroServiceException;

@RestController
@RequestMapping(value = "/autocompra/pagseguro")
@Scope(value = "request")
@CrossOrigin(origins = "*")
public class PagSeguro {

	private final String EMAIL = "msoftware.brasil@gmail.com";
	private final String TOKEN = "AB9F411C41344106A5D619ADD2FA6022";
	public String urlPagSeguro;
	public String msg;

	@RequestMapping(method = RequestMethod.PUT)
	ResponseEntity<String> criarPagamento(String nome, String email, String DDD, String fone,@RequestBody List<PagSeguroItemVO> objVO) {

		BigDecimal soma = new BigDecimal(0);
		try {
			PaymentRequest request = new PaymentRequest();
			for (PagSeguroItemVO obj : objVO) {
				Item item = new Item();
				item.setId(obj.getCodBarrasItem());
				item.setDescription(obj.getDescProd());
				item.setQuantity(obj.getQtdItens());
				BigDecimal valorItem = obj.getValorItem();
				soma = valorItem.add(soma);
				item.setAmount(soma);
			}
			request.setReference("AUTOCOMPRA");
			request.setCurrency(Currency.BRL);
			request.setSender(getSender(nome, email, DDD, fone));
			request.setNotificationURL("");
			request.setRedirectURL("");
			urlPagSeguro = request.register(getCredentials());

		} catch (Exception e) {
			Logger.getLogger(PagSeguro.class.getName()).log(Level.SEVERE, null, e);
			msg = e.getMessage();
			return ResponseEntity.ok().body(msg);
		}
		return ResponseEntity.ok().body("urlPagSeguro");
	}

	 private Credentials getCredentials() throws PagSeguroServiceException {
		return new AccountCredentials(EMAIL, TOKEN);
	}

	private Sender getSender(String nome, String email, String DDD, String fone) {
		Sender sender = new Sender();
		sender.setName(nome);
		sender.setEmail(email);
		sender.setPhone(new Phone(DDD, fone));

		return sender;
	}
}
