package com.msoftware.autocompraweb.controllers;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompraweb.service.impl.ClienteServiceImpl;
import com.msoftware.autocompraweb.vo.ClienteVO;

@RestController
@RequestMapping(value = "/autocompra/cliente")
@CrossOrigin(origins = "*")
public class Cliente {

	@Autowired
	private ClienteServiceImpl clienteService;

	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Void> insert(@RequestBody ClienteVO objVO) {
		this.clienteService.insert(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}

	@RequestMapping(method = RequestMethod.OPTIONS)
	public ResponseEntity<List<ClienteVO>> verifyClientebyEmail(String email, String provider) {
		List<ClienteVO> listObjVO = this.clienteService.verifyClientebyEmail(email,provider);
		return ResponseEntity.ok().body(listObjVO);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ClienteVO>> findIdCliente(String email, String provider, String firstName, String lastName) {
		List<ClienteVO> listObjVO = this.clienteService.findIdCliente(email , provider, firstName.toLowerCase(), lastName.toLowerCase());
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<List<ClienteVO>> update(@RequestBody ClienteVO objVO, @PathVariable Integer id) {
		objVO.setId(id);
		this.clienteService.update(objVO);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<ClienteVO>> deleteById(@PathVariable Integer id) {
		this.clienteService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
