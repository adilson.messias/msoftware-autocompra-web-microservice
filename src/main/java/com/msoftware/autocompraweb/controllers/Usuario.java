package com.msoftware.autocompraweb.controllers;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompraweb.service.impl.UsuarioServiceImpl;
import com.msoftware.autocompraweb.vo.UsuarioVO;

@RestController
@RequestMapping(value = "/autocompra/usuario")
@CrossOrigin(origins = "*")
public class Usuario {

	@Autowired
	private UsuarioServiceImpl usuarioService;

	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Void> insert(@RequestBody UsuarioVO objVO) {
		this.usuarioService.insert(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UsuarioVO>> findAllUsers() {
		List<UsuarioVO> listObjVO = this.usuarioService.findAllUsers();
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(method = RequestMethod.OPTIONS)
	public ResponseEntity<List<UsuarioVO>> findUserByEmail(UsuarioVO objVO) {
		String email = objVO.getEmail();
		List<UsuarioVO> listObjVO = this.usuarioService.findUserByEmail(email);
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<List<UsuarioVO>> update(@RequestBody UsuarioVO objVO, @PathVariable Integer id) {
		objVO.setId(id);
		this.usuarioService.update(objVO);
		return ResponseEntity.noContent().build();

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<UsuarioVO>> deleteById(@PathVariable Integer id) {
		this.usuarioService.deleteById(id);
		return ResponseEntity.noContent().build();

	}

}
