package com.msoftware.autocompraweb.controllers;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompraweb.service.impl.SalesItemServiceImpl;
import com.msoftware.autocompraweb.vo.SalesItemVO;



@RestController
@RequestMapping(value = "/autocompra/salesItem")
@CrossOrigin(origins = "*")
public class SalesItem {
	
	@Autowired
	private SalesItemServiceImpl service;
   
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<SalesItemVO>> findItemByCliente(SalesItemVO objVO) {
		List<SalesItemVO> listObjVO = this.service.findItemByCliente(objVO);
		return ResponseEntity.ok().body(listObjVO);

	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@RequestBody SalesItemVO objVO, String tableName) {
		this.service.insert(objVO , tableName);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}
	
}
