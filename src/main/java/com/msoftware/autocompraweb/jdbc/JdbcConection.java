package com.msoftware.autocompraweb.jdbc;

public class JdbcConection {

	private String url = "jdbc:postgresql://localhost:5432/postgres";
	private String user = "postgres";
	private String senha = "MEssi290496";

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
