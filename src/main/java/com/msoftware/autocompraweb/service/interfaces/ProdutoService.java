package com.msoftware.autocompraweb.service.interfaces;

import java.util.List;

import com.msoftware.autocompraweb.vo.ProdutoVO;

public interface ProdutoService {

	public void insert(ProdutoVO objVO);

	public List<ProdutoVO> findAllProdutos();

	public List<ProdutoVO> findProdutoById(Integer id);

	public void update(ProdutoVO objVO);

	public void deleteById(Integer id);
}
