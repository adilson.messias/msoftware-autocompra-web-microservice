package com.msoftware.autocompraweb.service.interfaces;

import java.util.List;

import com.msoftware.autocompraweb.vo.UsuarioVO;

public interface UsuarioService {
	
	public void insert(UsuarioVO objVO);
	
	public List<UsuarioVO> findAllUsers() ;
	
	public List<UsuarioVO> findUserByEmail(String email);
	
	public void update(UsuarioVO objVO);
	
	public void deleteById(Integer id);
}
