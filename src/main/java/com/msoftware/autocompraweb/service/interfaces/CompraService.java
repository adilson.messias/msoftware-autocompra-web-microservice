package com.msoftware.autocompraweb.service.interfaces;

import java.sql.SQLException;
import java.util.List;

import com.msoftware.autocompraweb.vo.CompraVO;

public interface CompraService {
	
	public void addItemCarrinho(CompraVO objVO) throws ClassNotFoundException, SQLException;
	
	public List<CompraVO> findAllItems(String table) throws ClassNotFoundException, SQLException;

	public void deleteById(CompraVO objVO);
}
