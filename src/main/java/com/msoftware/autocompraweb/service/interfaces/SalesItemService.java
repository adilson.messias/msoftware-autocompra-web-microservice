package com.msoftware.autocompraweb.service.interfaces;

import java.util.List;

import com.msoftware.autocompraweb.vo.SalesItemVO;

public interface SalesItemService {
	
	public List<SalesItemVO> findItemByCliente(SalesItemVO objVO);

}
