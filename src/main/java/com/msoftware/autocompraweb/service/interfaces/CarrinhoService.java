package com.msoftware.autocompraweb.service.interfaces;

import java.util.List;

import com.msoftware.autocompraweb.vo.CarrinhoVO;

public interface CarrinhoService {

	public List<CarrinhoVO> findAllCarrinho(Long table);
	
	public List<CarrinhoVO> findAll();

	public void deleteById(Integer id);
	
	
}
