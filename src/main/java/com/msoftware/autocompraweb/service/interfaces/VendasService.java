package com.msoftware.autocompraweb.service.interfaces;

import java.util.List;

import com.msoftware.autocompraweb.vo.VendasVO;

public interface VendasService {

	public List<VendasVO> findAllVendas();
	
	public void insert(VendasVO objVO);
}
