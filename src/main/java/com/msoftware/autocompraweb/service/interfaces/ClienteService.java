package com.msoftware.autocompraweb.service.interfaces;

import java.util.List;

import com.msoftware.autocompraweb.vo.ClienteVO;

public interface ClienteService {
	
	public void insert(ClienteVO objVO);
	
	public List<ClienteVO> verifyClientebyEmail(String email, String provider) ;
	
	public List<ClienteVO> findIdCliente(String email, String provider, String firstName, String lastName);
	
	public void update(ClienteVO objVO);
	
	public void deleteById(Integer id);
}
