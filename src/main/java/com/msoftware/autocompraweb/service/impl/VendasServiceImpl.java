package com.msoftware.autocompraweb.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.VendasEntity;
import com.msoftware.autocompraweb.repository.VendasRepository;
import com.msoftware.autocompraweb.service.interfaces.VendasService;
import com.msoftware.autocompraweb.vo.VendasVO;

@Repository
public class VendasServiceImpl implements VendasService {

	@Autowired
	private VendasRepository repo;

	public List<VendasVO> findAllVendas() {
		List<VendasEntity> list = (List<VendasEntity>) this.repo.findAllVendas();
		List<VendasVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	private VendasVO fromVO(VendasEntity entity) {
		VendasVO objVO = new VendasVO(entity.getId(), entity.getData(),entity.getTableName(),
				entity.getIdCliente(),entity.getIdItens(), entity.getValorTotal());
	   return objVO;

	}

	@Override
	public void insert(VendasVO objVO) {
		Date data =  new Date();
		objVO.setData(data);
		VendasEntity entity = fromEntity(objVO);
		this.repo.save(entity);
		
						
	}

	private VendasEntity fromEntity(VendasVO objVO) {
		return new VendasEntity(objVO.getId(), objVO.getData(), objVO.getTableName(),objVO.getIdCliente(), objVO.getIdItens(), objVO.getValorTotal());

	}

	
		
}
