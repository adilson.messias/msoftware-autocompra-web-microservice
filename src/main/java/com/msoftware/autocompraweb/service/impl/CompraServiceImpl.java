package com.msoftware.autocompraweb.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.CompraEntity;
import com.msoftware.autocompraweb.repository.CarrinhoRepository;
import com.msoftware.autocompraweb.repository.CompraRepository;
import com.msoftware.autocompraweb.service.interfaces.CompraService;
import com.msoftware.autocompraweb.vo.CarrinhoVO;
import com.msoftware.autocompraweb.vo.CompraVO;

@Repository
public class CompraServiceImpl implements CompraService {

	public void addItemCarrinho(CompraVO objVO) throws ClassNotFoundException, SQLException {
		CompraRepository.addItemCarrinho(objVO);

	}

	public List<CompraVO> findAllItems(String table) {
		List<CompraEntity> list = (List<CompraEntity>) CompraRepository.findAllItems(table);
		List<CompraVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	private CompraVO fromVO(CompraEntity entity) {
		CompraVO objVO = new CompraVO(entity.getId(), entity.getCodBarrasProd(), entity.getDescProd(),
				entity.getPrecoUnit(), entity.getPesoProd(), entity.getQtdProd(), entity.getValorTotal(),
				entity.getTableName(), entity.getCodCliente(), entity.getData());
		return objVO;

	}

	public void deleteById(CompraVO objVO) {
		CompraRepository.deleteItemCarrinho(objVO);
		
	}
	
	public void update(CarrinhoVO objVO) {
		CarrinhoRepository.updateStatusCarrinho(objVO);

		
	}


}
