package com.msoftware.autocompraweb.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.CarrinhoEntity;
import com.msoftware.autocompraweb.repository.CarrinhoRepository;
import com.msoftware.autocompraweb.service.interfaces.CarrinhoService;
import com.msoftware.autocompraweb.vo.CarrinhoVO;

@Repository
public class CarrinhoServiceImpl implements CarrinhoService {

	@Autowired
	private CarrinhoRepository repo;

	public List<CarrinhoVO> findAllCarrinho(Long table) {
		List<CarrinhoEntity> list = (List<CarrinhoEntity>) CarrinhoRepository.findAllCarrinho(table);
		List<CarrinhoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	private CarrinhoVO fromVO(CarrinhoEntity entity) {
		CarrinhoVO objVO = new CarrinhoVO(entity.getId(), entity.getCodBarras(), entity.getStatus());
		return objVO;

	}

	public void insert(Long table) throws ClassNotFoundException, SQLException {
		CarrinhoRepository.createTableCarrinho(table);
		CarrinhoVO objVO = new CarrinhoVO();
		objVO.setCodBarras(table);
		objVO.setStatus("Inoperante");
		this.save(objVO);
	}

	public void save(CarrinhoVO objVO) {
		CarrinhoEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	private CarrinhoEntity fromEntity(CarrinhoVO objVO) {
		return new CarrinhoEntity(objVO.getId(), objVO.getCodBarras(), objVO.getStatus());

	}

	public List<CarrinhoVO> findAll() {
		List<CarrinhoEntity> list = (List<CarrinhoEntity>) this.repo.findAll();
		List<CarrinhoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}
	
	@Override
	public void deleteById(Integer id) {
		CarrinhoRepository.dropTableCarrinho(id);
		this.repo.delete(id);

				
	}

	public void update(CarrinhoVO objVO) {
		CarrinhoRepository.updateStatusCarrinho(objVO);
	}

}
