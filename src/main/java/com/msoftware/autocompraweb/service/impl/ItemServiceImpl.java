package com.msoftware.autocompraweb.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.ItemEntity;
import com.msoftware.autocompraweb.repository.ItemRepository;
import com.msoftware.autocompraweb.service.interfaces.ItemService;
import com.msoftware.autocompraweb.vo.ItemVO;

@Repository
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemRepository repo;

	public List<ItemVO> findItemByCodeBarra(Long code) {
		List<ItemEntity> list = (List<ItemEntity>) this.repo.findItemByCodeBarra(code);
		List<ItemVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	
	private ItemVO fromVO(ItemEntity entity) {
		ItemVO objVO = new ItemVO(entity.getId(), entity.getCodBarrasProd(), entity.getDescProd(), entity.getPrecoUnit(), entity.getPesoProd());
		return objVO;

	}

		
}
