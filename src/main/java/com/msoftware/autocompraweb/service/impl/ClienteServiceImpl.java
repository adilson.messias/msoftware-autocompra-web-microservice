package com.msoftware.autocompraweb.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.ClienteEntity;
import com.msoftware.autocompraweb.repository.ClienteRepository;
import com.msoftware.autocompraweb.service.interfaces.ClienteService;
import com.msoftware.autocompraweb.vo.ClienteVO;

@Repository
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository repo;

	@Override
	public void insert(ClienteVO objVO) {
		ClienteEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	private ClienteEntity fromEntity(ClienteVO objVO) {
		return new ClienteEntity(objVO.getId(), objVO.getEmailPadrao(), objVO.getSenhaPadrao(), objVO.getName(),
				objVO.getEmailGoogle(), objVO.getPhotoUrlGoogle(), objVO.getEmailFacebook(),
				objVO.getPhotoUrlFacebook(), objVO.getFirstName().toLowerCase(), objVO.getLastName().toLowerCase(),
				objVO.getAuthToken(), objVO.getProvider().toLowerCase());

	}

	private ClienteVO fromVO(ClienteEntity entity) {
		ClienteVO objVO = new ClienteVO(entity.getId(), entity.getEmailPadrao(), entity.getSenhaPadrao(),
				entity.getName(), entity.getEmailGoogle(), entity.getPhotoUrlGoogle(), entity.getEmailFacebook(),
				entity.getPhotoUrlFacebook(), entity.getFirstName(), entity.getLastName(), entity.getAuthToken(),
				entity.getProvider().toLowerCase());

		return objVO;
	}

	@Override
	public List<ClienteVO> findIdCliente(String email, String provider, String firstName, String lastName) {
		List<ClienteVO> listObjVO = new ArrayList<ClienteVO>();
		List<ClienteEntity> list = (List<ClienteEntity>) ClienteRepository.findIdCliente(email, provider, firstName,
				lastName);
		listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());

		return listObjVO;
	}

	@Override
	public void update(ClienteVO objVO) {
		ClienteEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	@Override
	public void deleteById(Integer id) {
		this.repo.delete(id);

	}

	public List<ClienteVO> verifyClientebyEmail(String email, String provider) {
		List<ClienteEntity> list = (List<ClienteEntity>) ClienteRepository.verifyClientebyEmail(email, provider);
		List<ClienteVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

}
