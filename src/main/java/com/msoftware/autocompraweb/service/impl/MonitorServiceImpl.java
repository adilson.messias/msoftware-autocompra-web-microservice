package com.msoftware.autocompraweb.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.CarrinhoEntity;
import com.msoftware.autocompraweb.entity.CompraEntity;
import com.msoftware.autocompraweb.repository.CompraRepository;
import com.msoftware.autocompraweb.repository.MonitorRepository;
import com.msoftware.autocompraweb.service.interfaces.MonitorService;
import com.msoftware.autocompraweb.vo.CarrinhoVO;
import com.msoftware.autocompraweb.vo.CompraVO;

@Repository
public class MonitorServiceImpl implements MonitorService {

	@Autowired
	private MonitorRepository repo;

	public List<CarrinhoVO> findAllOperante() {
		List<CarrinhoEntity> list = (List<CarrinhoEntity>) this.repo.findAllOperante();
		List<CarrinhoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	private CarrinhoVO fromVO(CarrinhoEntity entity) {
		CarrinhoVO objVO = new CarrinhoVO(entity.getId(), entity.getCodBarras(), entity.getStatus());
		return objVO;

	}
	
	public List<CompraVO> findAllItems(String table) {
		List<CompraEntity> list = (List<CompraEntity>) CompraRepository.findAllItems(table);
		List<CompraVO> listObjVO = list.stream().map(entity -> fromCompraVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}
	
	private CompraVO fromCompraVO(CompraEntity entity) {
		CompraVO objVO = new CompraVO(entity.getId(), entity.getCodBarrasProd(), entity.getDescProd(),
				entity.getPrecoUnit(), entity.getPesoProd(), entity.getQtdProd(), entity.getValorTotal(),
				entity.getTableName(), entity.getCodCliente(), entity.getData());
		return objVO;

		
	}

	
}
