package com.msoftware.autocompraweb.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.SalesItemEntity;
import com.msoftware.autocompraweb.repository.SalesItemRepository;
import com.msoftware.autocompraweb.service.interfaces.SalesItemService;
import com.msoftware.autocompraweb.vo.SalesItemVO;

@Repository
public class SalesItemServiceImpl implements SalesItemService {

	@Autowired
	private SalesItemRepository repo;

	@Override
	public List<SalesItemVO> findItemByCliente(SalesItemVO objVO) {
		List<SalesItemEntity> list = (List<SalesItemEntity>) SalesItemRepository.findItemByCliente(objVO);
		List<SalesItemVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	private SalesItemVO fromVO(SalesItemEntity entity) {
		SalesItemVO objVO = new SalesItemVO(entity.getId(), entity.getIdCliente(),entity.getIdVenda(), entity.getCodBarrasProd(),
				entity.getDescProd(), entity.getPrecoUnit(), entity.getQtdItens(), entity.getValorTotal(),
				entity.getIdItens(), entity.getPesoProd(), entity.getPesoCarrinho());

		return objVO;

	}

	public void insert(SalesItemVO objVO, String tableName ) {
		SalesItemEntity obj = SalesItemRepository.findMaxIdVenda(tableName);
		objVO.setIdVenda(obj.getIdVenda());
		SalesItemEntity entity = fromEntity(objVO);
		this.repo.save(entity);
	
	}

	private SalesItemEntity fromEntity(SalesItemVO objVO) {
		return new SalesItemEntity(objVO.getId(), objVO.getIdCliente(), objVO.getIdVenda(), objVO.getCodBarrasProd(),
				objVO.getDescProd(), objVO.getPrecoUnit(), objVO.getQtdItens(), objVO.getValorTotal(),
				objVO.getIdItens(), objVO.getPesoProd(), objVO.getPesoCarrinho());

	}

}
