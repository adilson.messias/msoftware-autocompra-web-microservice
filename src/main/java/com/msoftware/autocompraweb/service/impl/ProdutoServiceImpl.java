package com.msoftware.autocompraweb.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.ProdutoEntity;
import com.msoftware.autocompraweb.repository.ProdutoRepository;
import com.msoftware.autocompraweb.service.interfaces.ProdutoService;
import com.msoftware.autocompraweb.vo.ProdutoVO;

@Repository
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository repo;

	@Override
	public void insert(ProdutoVO objVO) {
		ProdutoEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	private ProdutoEntity fromEntity(ProdutoVO objVO) {
		return new ProdutoEntity(objVO.getId(), objVO.getCodBarrasProd(), objVO.getDescProd(), objVO.getPrecoUnit(), objVO.getPesoProd());

	}

	@Override
	public List<ProdutoVO> findAllProdutos() {
		List<ProdutoEntity> list = (List<ProdutoEntity>) this.repo.findAllProdutos();
		List<ProdutoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}
	
	private ProdutoVO fromVO(ProdutoEntity entity) {
		ProdutoVO objVO = new ProdutoVO(entity.getId(), entity.getCodBarrasProd(), entity.getDescProd(), entity.getPrecoUnit(), entity.getPesoProd());
		return objVO;

	}

	@Override
	public void update(ProdutoVO objVO) {
		ProdutoEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	@Override
	public void deleteById(Integer id) {
		this.repo.delete(id);
	}

	@Override
	public List<ProdutoVO> findProdutoById(Integer id) {
		List<ProdutoEntity> list = (List<ProdutoEntity>) this.repo.findProdutoById(id);
		List<ProdutoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

}
