package com.msoftware.autocompraweb.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompraweb.entity.UsuarioEntity;
import com.msoftware.autocompraweb.repository.UsuarioRepository;
import com.msoftware.autocompraweb.service.interfaces.UsuarioService;
import com.msoftware.autocompraweb.vo.UsuarioVO;

@Repository
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository repo;

	@Override
	public void insert(UsuarioVO objVO) {
		UsuarioEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	private UsuarioEntity fromEntity(UsuarioVO objVO) {
		return new UsuarioEntity(objVO.getEmail(), objVO.getSenha(), objVO.getId(), objVO.getMatricula(), objVO.getPerfil());

	}

	private UsuarioVO fromVO(UsuarioEntity entity) {
		UsuarioVO objVO = new UsuarioVO(entity.getEmail(), entity.getSenha(), entity.getId(), entity.getMatricula(), entity.getPerfil());
		return objVO;

	}

	@Override
	public List<UsuarioVO> findUserByEmail(String email) {
		List<UsuarioEntity> list = (List<UsuarioEntity>) this.repo.findUserByEmail(email);
		List<UsuarioVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	@Override
	public void update(UsuarioVO objVO) {
		UsuarioEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	@Override
	public void deleteById(Integer id) {
		this.repo.delete(id);

	}

	public List<UsuarioVO> findAllUsers() {
		List<UsuarioEntity> list = (List<UsuarioEntity>) this.repo.findAllUsers();
		List<UsuarioVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

}
