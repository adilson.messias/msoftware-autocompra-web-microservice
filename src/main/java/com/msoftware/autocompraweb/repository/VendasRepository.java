package com.msoftware.autocompraweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msoftware.autocompraweb.entity.VendasEntity;
import com.msoftware.autocompraweb.jdbc.JdbcConection;

public interface VendasRepository extends CrudRepository<VendasEntity, Integer> {
	JdbcConection conection = new JdbcConection();

	@Query(nativeQuery = true, value = "select * from tb_vendas order by id")
	public List<VendasEntity> findAllVendas();

	}
