package com.msoftware.autocompraweb.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.msoftware.autocompraweb.entity.SalesItemEntity;
import com.msoftware.autocompraweb.jdbc.JdbcConection;
import com.msoftware.autocompraweb.vo.SalesItemVO;

public interface SalesItemRepository extends CrudRepository<SalesItemEntity, Integer> {
	JdbcConection conection = new JdbcConection();

	public static List<SalesItemEntity> findItemByCliente(SalesItemVO objVO) {
		ArrayList<SalesItemEntity> list = new ArrayList<SalesItemEntity>();
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select * from  tb_sales_item "
					   + "where id_venda = " + objVO.getId() + " and id_itens = " + objVO.getIdItens() + "order by id"; 
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				SalesItemEntity item = new SalesItemEntity();
				item.setId(rs.getInt("id"));
				item.setIdCliente(rs.getInt("id_cliente"));
				item.setCodBarrasProd(rs.getLong("cod_barra_prod"));
				item.setDescProd(rs.getString("desc_prod"));
				item.setPrecoUnit(rs.getDouble("preco_unit"));
				item.setPesoProd(rs.getDouble("peso_prod"));
				item.setQtdItens(rs.getInt("qtd_itens"));
				item.setValorTotal(rs.getDouble("valor_total"));
				list.add(item);
			}
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;

	}

	public static SalesItemEntity findMaxIdVenda(String tableName){
		SalesItemEntity obj = new SalesItemEntity();
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select Max(id) from  tb_vendas";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
			  obj.setIdVenda(rs.getInt("max"));
			}
			rs.close();
			String sql1 = " delete from " +  "\"" + tableName + "\" " ;
			//+ "\"" + objVO.getTableName();
			stmt.executeUpdate(sql1);
			stmt.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return obj;
		
	}
	
}
