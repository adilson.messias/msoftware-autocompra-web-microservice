package com.msoftware.autocompraweb.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.msoftware.autocompraweb.entity.ClienteEntity;
import com.msoftware.autocompraweb.jdbc.JdbcConection;

public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
	JdbcConection conection = new JdbcConection();

	public static List<ClienteEntity> verifyClientebyEmail(String email, String provider) {
		ArrayList<ClienteEntity> list = new ArrayList<ClienteEntity>();
		String providerPesquisa = "";
		if (provider.equals("google")) {
			providerPesquisa = "email_google";
		} else if (provider.equals("facebook")) {
			providerPesquisa = "email_facebook";
		} else {
			providerPesquisa = "email_padrao";
		}
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select * from tb_cliente where " + providerPesquisa + " = " + '\'' + email + '\'' + "";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ClienteEntity item = new ClienteEntity();
				item.setId(rs.getInt("id"));
				item.setEmailPadrao(rs.getString("email_padrao"));
				item.setSenhaPadrao(rs.getString("senha_padrao"));
				item.setName(rs.getString("name"));
				item.setEmailGoogle(rs.getString("email_google"));
				item.setPhotoUrlGoogle(rs.getString("photo_url_google"));
				item.setEmailFacebook(rs.getString("email_facebook"));
				item.setPhotoUrlFacebook(rs.getString("photo_url_facebook"));
				item.setFirstName(rs.getString("first_name"));
				item.setLastName(rs.getString("last_name"));
				item.setAuthToken(rs.getString("auth_token"));
				item.setProvider(rs.getString("provider"));
				list.add(item);
			}
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	
	public static List<ClienteEntity> findIdCliente(String email, String provider, String firstName, String lastName) {
		ArrayList<ClienteEntity> list = new ArrayList<ClienteEntity>();
		//String emailPesquisa = "";
		//if (provider.equals("google")) {
		//	emailPesquisa = "email_google";
		//} else if (provider.equals("facebook")) {
		//	emailPesquisa = "email_facebook";
		//} else {
		//	emailPesquisa = "email_padrao";
		//}
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select * from tb_cliente where id =(select MIN(id) as id from tb_cliente "
					+ "where first_name = " + '\'' + firstName + '\'' + " and last_name = " + '\'' + lastName + '\''+ ")";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ClienteEntity item = new ClienteEntity();
				item.setId(rs.getInt("id"));
				item.setEmailPadrao(rs.getString("email_padrao"));
				item.setSenhaPadrao(rs.getString("senha_padrao"));
				item.setName(rs.getString("name"));
				item.setEmailGoogle(rs.getString("email_google"));
				item.setPhotoUrlGoogle(rs.getString("photo_url_google"));
				item.setEmailFacebook(rs.getString("email_facebook"));
				item.setPhotoUrlFacebook(rs.getString("photo_url_facebook"));
				item.setFirstName(rs.getString("first_name"));
				item.setLastName(rs.getString("last_name"));
				item.setAuthToken(rs.getString("auth_token"));
				item.setProvider(rs.getString("provider"));
				list.add(item);
			}
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
}