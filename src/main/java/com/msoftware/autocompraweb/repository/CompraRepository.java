package com.msoftware.autocompraweb.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.msoftware.autocompraweb.entity.CompraEntity;
import com.msoftware.autocompraweb.jdbc.JdbcConection;
import com.msoftware.autocompraweb.vo.CompraVO;

public interface CompraRepository {
	JdbcConection conection = new JdbcConection();

	public static void addItemCarrinho(CompraVO objVO) {
		CompraEntity entity = new CompraEntity();
		Integer id = 0;
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select id from" + "\"" + objVO.getTableName() + "\"" + "";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				entity.setId(rs.getInt("id"));
			}
			if (entity.getId() == null)
				entity.setId(0);
			id = entity.getId() + 1;
			rs.close();
			String sql1 = "INSERT INTO " + "\"" + objVO.getTableName() + "\"" + "" + "(id," + "cod_barra_prod,"
					+ "desc_prod," + "preco_unit," + "peso_prod," + "qtd_prod," + "valor_total," + "table_name,"
					+ "cod_cli," + "data) " + "VALUES" + "(" + id + "," + "" + objVO.getCodBarrasProd() + "," + ""
					+ '\'' + objVO.getDescProd() + '\'' + "," + "" + objVO.getPrecoUnit() + "," + ""
					+ objVO.getPesoProd() + "," + "" + objVO.getQtdProd() + "," + "" + objVO.getValorTotal() + "," + ""
					+ objVO.getTableName() + "," + "" + objVO.getCodCliente() + "," + "" + '\'' + objVO.getData() + '\''
					+ ")";
			stmt.executeUpdate(sql1);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static List<CompraEntity> findAllItems(String table) {
		ArrayList<CompraEntity> list = new ArrayList<CompraEntity>();
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select * from " + "\"" + table + "\" " + "order by id";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				CompraEntity item = new CompraEntity();
				item.setId(rs.getInt("id"));
				item.setCodBarrasProd(rs.getLong("cod_barra_prod"));
				item.setDescProd(rs.getString("desc_prod"));
				item.setPrecoUnit(rs.getDouble("preco_unit"));
				item.setPesoProd(rs.getDouble("peso_prod"));
				item.setQtdProd(rs.getInt("qtd_prod"));
				item.setValorTotal(rs.getDouble("valor_total"));
				item.setTableName(rs.getLong("table_name"));
				item.setCodCliente(rs.getInt("cod_cli"));
				item.setData(rs.getDate("data"));
				list.add(item);
			}
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;

	}

	public static void deleteItemCarrinho(CompraVO objVO) {
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = " delete from " + "\"" + objVO.getTableName() + "\" where id =" + objVO.getId();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	

}
