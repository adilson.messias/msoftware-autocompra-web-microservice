package com.msoftware.autocompraweb.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.msoftware.autocompraweb.entity.ItemEntity;
import com.msoftware.autocompraweb.jdbc.JdbcConection;

public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
	JdbcConection conection = new JdbcConection();

	@Query(nativeQuery = true, value = "select * from tb_produto where cod_barra_prod in(:code)")
	public List<ItemEntity> findItemByCodeBarra(@Param("code") Long code);

	

}
