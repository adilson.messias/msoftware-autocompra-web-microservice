package com.msoftware.autocompraweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.msoftware.autocompraweb.entity.ProdutoEntity;


public interface ProdutoRepository extends CrudRepository<ProdutoEntity, Integer> {

	@Query(nativeQuery = true, value = "select * from tb_produto where id in(:id)")
	public List<ProdutoEntity> findProdutoById(@Param("id") Integer id);

	@Query(nativeQuery = true, value = "select * from tb_produto order by id")
	public List<ProdutoEntity> findAllProdutos();

	
	
}
