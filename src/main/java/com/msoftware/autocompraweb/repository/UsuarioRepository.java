package com.msoftware.autocompraweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.msoftware.autocompraweb.entity.UsuarioEntity;

public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

	@Query(nativeQuery = true, value = "select * from tb_login_user where email in(:email)")
	public List<UsuarioEntity> findUserByEmail(@Param("email") String email);

	@Query(nativeQuery = true, value = "select * from tb_login_user order by id")
	public List<UsuarioEntity> findAllUsers();

		
}
