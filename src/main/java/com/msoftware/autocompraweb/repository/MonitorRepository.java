package com.msoftware.autocompraweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.msoftware.autocompraweb.entity.CarrinhoEntity;

public interface MonitorRepository extends CrudRepository<CarrinhoEntity, Integer> {

	@Query(nativeQuery = true, value = "select * from tb_carrinho_fisico  where status = 'Em Operação' order by id")
	public List<CarrinhoEntity> findAllOperante();

}
