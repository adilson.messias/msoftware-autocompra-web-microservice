package com.msoftware.autocompraweb.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.msoftware.autocompraweb.entity.CarrinhoEntity;
import com.msoftware.autocompraweb.jdbc.JdbcConection;
import com.msoftware.autocompraweb.vo.CarrinhoVO;

public interface CarrinhoRepository extends CrudRepository<CarrinhoEntity, Integer> {
	JdbcConection conection = new JdbcConection();

	@Query(nativeQuery = true, value = "select * from tb_carrinho_fisico order by id")
	public List<CarrinhoEntity> findAll();

	public static List<CarrinhoEntity> findAllCarrinho(Long table) {
		ArrayList<CarrinhoEntity> list = new ArrayList<CarrinhoEntity>();
		CarrinhoEntity item = new CarrinhoEntity();

		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select * from " + "\"" + table + "\" " + "order by id";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setCodBarras(rs.getLong("cod_barras"));
				item.setStatus(rs.getString("status"));
				list.add(item);
			}
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void createTableCarrinho(Long table) throws ClassNotFoundException, SQLException {

		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = " create table " + "\"" + table + "\" " + " " + "(id Integer not NULL, "
					+ " cod_barra_prod bigint , " + " desc_prod Varchar(255), " + " cod_cli Integer, "
					+ " peso_prod Real, " + " preco_unit Real, " + " qtd_prod Integer, " + " table_name bigint, "
					+ " valor_total Real, " + "data Date ," + " PRIMARY KEY ( id ))";

			System.out.printf(sql);
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void dropTableCarrinho(Integer id) {
		CarrinhoEntity item = new CarrinhoEntity();
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "select cod_barras from tb_carrinho_fisico where id = " + id;
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				item.setCodBarras(rs.getLong("cod_barras"));
			}
			rs.close();
			String sql1 = " drop table " + "\"" + item.getCodBarras() + "\" ";
			stmt.executeUpdate(sql1);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void updateStatusCarrinho(CarrinhoVO objVO) {
		try {
			Connection connection = DriverManager.getConnection(conection.getUrl(), conection.getUser(),
					conection.getSenha());
			Statement stmt = connection.createStatement();
			String sql = "UPDATE tb_carrinho_fisico "
					    +"SET status = " + '\'' + objVO.getStatus() + '\''+ " "
						+ "where cod_Barras= " + '\'' + objVO.getCodBarras() + '\'';	         
			System.out.printf(sql);
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
